<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>Buat Account Baru</h1>
    <form action="/welcome" method="post">
        @csrf
    <h2>Sign Up Form</h2>
    <label>First Name :</label><br>
    <input type="text" name="first"> <br><br>
    <label >Last Name :</label> <br>
    <input type="text" name="last"> <br><br>
    <label >Gender :</label> <br>
    <input type="radio"name="gender" value="1"> Male <br>
    <input type="radio"name="gender" value="2"> Female <br>
    <input type="radio"name="gender" value="3"> Other <br><br>
    <label >Nationality :</label><br>
    <select name="nationality" >
        <option value="ina">Indonesia</option> 
        <option value="jpy">Japan</option>
        <option value="sgp">Singapore</option>
        <option value="irn">Iran</option>
    </select> <br><br>
    <label >Language Spoken :</label> <br>
    <input type="checkbox"name ="lang" value="1">Bahasa Indonesia <br>
    <input type="checkbox"name ="lang" value="2">English <br>
    <input type="checkbox"name ="lang" value="3">Other <br><br>
    <label >Bio :</label><br>
    <textarea name="bio" cols="30" rows="4"></textarea><br><br>
    <input type="submit"value="kirim">
</form>
</body>
</html>