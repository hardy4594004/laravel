@extends('layout.master')

@section('judul')
    <h1> Halaman Tambah Cast</h1>
@endsection

@section('content')

<form action="/cast" method="post"> 
@csrf
  <div class="form-group">
    <label>Nama Cast</label>
    <input type="text" name="nama" class="form-control">
     </div>
     @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label>Umur Cast</label>
    <input type="text" name="umur" class="form-control">
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label>Bio Cast</label>
    <textarea name="bio" class="form-control" cols="20" rows="5"></textarea>
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection