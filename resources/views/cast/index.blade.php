@extends('layout.master')

@section('judul')
    <h1> Halaman Daftar Cast</h1>
@endsection

@section('content')
<a href="/cast/create" class="btn btn-primary bt-sm mb-3">Tambah Cast</a>

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
  @forelse ($cast as $key => $value)
   <tr>
    <td>{{$key+1}}</td>
    <td>{{$value->nama}}</td>
    <td>{{$value->umur}}</td>
    <td>
        <form action="/cast/{{$value->id}}" method="post">
            @csrf
            @method ('DELETE')
            <a href="/cast/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
            <a href="/cast/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>

        <input type="submit" value="delete" class="btn btn-danger btn-sm">
    </form>
    </td>
   </tr>
    @empty
    <tr>
        <td>Tidak Ada Data</td>
    </tr>
    @endforelse
  </tbody>
  </table>


@endsection