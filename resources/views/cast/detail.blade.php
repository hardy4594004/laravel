@extends('layout.master')

@section('judul')
    <h1> Detail Daftar Cast</h1>
@endsection

@section('content')
<h1>Nama : {{$cast-> nama }}</h1>
<h1>Umur : {{$cast-> umur }}</h1>
<h3>Bio : {{$cast-> bio }}</h3>

<a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>

@endsection