<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController; // panggil cast controller di sini
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'beranda']);

Route::get('/register', [HomeController::class, 'daftar']);

Route::post('/welcome', [AuthController::class, 'kirim']);
Route::get('/table', function(){
    return view('table.table');
});

Route::get('/data-table', function(){
    return view('table.data-table');
});

// CRUD CAST
// A. 1. Create
// buat route terlebih dahulu untuk mengarah ke form tambah CAST
// kemudian buat controller dengan nama CastController setelah itu use di atas 
// dan masukkan ke route beserta calssnya lalu buat fungsinya yaitu create untuk mengarah ke form tambah cast


route::get('/cast/create',[CastController::class, 'create']);

//A. 2. tambah/simpan
// buat route untuk mengirim data yang diinput oleh form ke database atau tambah data ke data base
// karena ingin memasukkan data ke data base maka kita gunakan store
// post ini di muat ke action pada form add.blade.php --->  <form action="/cast"> dengan method store
// jangan lupa setiap form yg ada input sabmitnya tambhakan @csrf sebagai token nya
// lalu buat fungsi dengan mana store ini do controlle

route::post('/cast',[CastController::class, 'store']);

// B.1. Read
// buat route untuk menampilhan data yang d=sudah ditambahkan ke dalam database dengan menggunakan methode index
// selanjutnya tambhan methode tersebut di definisakan di halaman controller
route::get('/cast',[CastController::class, 'index']);

// B.2. Detail
// detail cast bedasarkan id
route::get('/cast/{cast_id}',[CastController::class, 'detail']);


// C. 1. Update - Edit
// buat route menuju form update cast

route::get('/cast/{cast_id}/edit',[CastController::class, 'edit']);

// C.2. Update - Memasukkan Kembali Ke dadtabase

route::put('/cast/{cast_id}',[CastController::class, 'update']);

// D.1. Delete
// delete berdasarkan parameter id
route::delete('/cast/{cast_id}',[CastController::class, 'destroy']);