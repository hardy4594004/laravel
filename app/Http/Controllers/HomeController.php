<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class HomeController extends Controller
{
    public function beranda () {
        return view('home');
    }

    public function daftar () {
        return view('register');
    }

    
}
