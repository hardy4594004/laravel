<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function kirim (Request $request) 
    {
        $first = $request ['first'];
        $last = $request ['last'];
        $gender=$request['gender'];
        $nationality=$request ['nationality'];
        $lang=$request ['lang'];
        $bio=$request['bio'];

        return view ('welcome',
    [
    'first' => $first,
    'last' => $last,
    'gender'=> $gender,
    'nationality'=> $nationality,
    'lang'=> $lang,
    'bio'=> $bio,
    ]);
    }
}
