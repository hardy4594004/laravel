<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; // fungsi querry bulder 

class CastController extends Controller
{
    //setelah di buat fungsi crrate di route masukkan coding untuk nnati memanggil halaman create
    // yg dibuat di folder pada bagian views berupa sub folder cast dan file add.blade.php

    public function create ()
    {
        return view('cast.add');
    }

    // buat fungsi stode dimana dia menerima Request berupa request namenya
    public function store (Request $request)
    {

        // karena pada tabel terdapat null yg tidak boleh kososng maka kita buat dulu validation
        // agar nnt saat form tidak diisi kita bisa kasih allert/peringatan

        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',


        // lalu dilanjutkan dengan memebuat pesan error pada bagian add.blade.php
        // namun jika semua diisi sesuai syarat maka perlu dibuat proses tambah datanya dengan menambahkan fungsi querry bulder pada bagia atas halaman ini
        // selnjutnya untuk tambah data kita buat insert statement
        
        ]);

        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        // table yg ingin di tambah adalah table "cast" yang ada didatabese
        // isiannya bedasarkan request yg diinput di nama, request di umur dst
        // selanjutnya  buat return redirect ke halaman yang diinginkan

        return redirect('/cast');
    }

    // fungsi ini ibuat unruk memanggil route index yang telah dibuat sebelumnya
    // lalu kebalikan view ke halaman index denganmembuat file baru di folder cast dengan nama index.blade.php
    public function index ()
    {
        $cast = DB::table('cast')->get();
 
        return view('cast.index', ['cast' => $cast]);

 
    }

    public function detail ($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.detail',['cast'=>$cast]);
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit',['cast'=>$cast]);
    }

    public function update (Request $request,$id)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('cast')
              ->where('id', $id)
              ->update(
                [
                    'nama' => $request->nama,
                    'umur' => $request->umur,
                    'bio' => $request->bio
                ],
            );

            return redirect('/cast');

    }

    public function destroy ($id)
    {
        DB::table('cast')->where ('id',$id)->delete();
        return redirect('/cast');
    }
}
